
package clases;

import javax.swing.JOptionPane;

/**Nombre de la clase: Ejercicio2
 *Fecha: 24/05/2018
 * @author Kevin Lovos
 * Copyright: Itca Fepade
 * Version: 1.0
 */
public class Ejercicio2 implements AccionesEj2 {
    private double lado1=0.0;
    private double lado2=0.0;
    private static double area=0;
    //implementando interfaces
    @Override
    public void calcularArea(String tipo){
        switch(tipo){
           
            //rectangulo, triangulos, rombo y romboide
            case "rectangulo":
                //en este caso val1 y val2 se usan como lados diferentes delrectangulo 
               area = this.lado1 * this.lado2;
                JOptionPane.showMessageDialog(null, "El area de el Rectangulo es: " + area);
                break;
            case "triangulo":
                //en este caso val1 y val2 se usan como base y altura de el triangulo
               area = (this.lado1 * this.lado2)/2;
                JOptionPane.showMessageDialog(null, "El area de el Triangulo es: " + area);
                break;
            case "rombo":
                //en este caso val1 y val2 se usan como las diagonales del rombo
                area =(this.lado1 * this.lado2)/2;
                JOptionPane.showMessageDialog(null, "El area de el Rombo es: " + area);
                break;
            case "romboide":
               area = this.lado1 * this.lado2;
                JOptionPane.showMessageDialog(null, "El area de el Romboide es: " + area);
                break;
        }
    }
     @Override
     public void mostrarResultado(){
         
     }

    public double getLado1() {
        return lado1;
    }

    public void setLado1(double lado1) {
        this.lado1 = lado1;
    }

    public double getLado2() {
        return lado2;
    }

    public void setLado2(double lado2) {
        this.lado2 = lado2;
    }

    public static double getArea() {
        return area;
    }

    public static void setArea(double area) {
        Ejercicio2.area = area;
    }
   
    
}
