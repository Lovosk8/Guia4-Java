package clases;

/**Fecha: 24/05/2018
 * @author Kevin Lovos
 * Copyright: Itca Fepade
 * Version: 1.0
*Nombre de la clase: AccionesEj2
 */
public interface AccionesEj2 {
    public void calcularArea(String tipo);
    public void mostrarResultado();
}
