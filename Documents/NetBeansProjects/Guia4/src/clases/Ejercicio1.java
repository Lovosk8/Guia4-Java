package clases;

import javax.swing.JOptionPane;

/**Nombre de la clase: Ejercicio1
 *Fecha: 24/05/2018
 * @author Kevin Lovos
 * Copyright: Itca Fepade
 * Version: 1.0
 */
public class Ejercicio1 {
    private double num1;
    private double num2;
    private double resultado;
    //metodos constructores de la clase
    public Ejercicio1() {
    }

    public Ejercicio1(double num1, double num2, double resultado) {
        this.num1 = num1;
        this.num2 = num2;
        this.resultado = resultado;
    }

    public double getNum1() {
        return num1;
    }

    public void setNum1(double num1) {
        this.num1 = num1;
    }

    public double getNum2() {
        return num2;
    }

    public void setNum2(double num2) {
        this.num2 = num2;
    }

    public double getResultado() {
        return resultado;
    }

    public void setResultado(double resultado) {
        this.resultado = resultado;
    }
    
    
    public void sumar(){
        this.resultado = this.num1+this.num2;
        JOptionPane.showMessageDialog(null, "El resutado de la suma es:" + this.resultado);
    }
    public double sumar(double nume1, double nume2){
        this.num1 = nume1;
        this.num2 = nume2;
        this.resultado = this.num1 + this.num2;
        return this.resultado;
    }
}
