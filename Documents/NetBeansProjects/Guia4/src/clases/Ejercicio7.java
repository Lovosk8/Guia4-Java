
package clases;

import javax.swing.JOptionPane;

/**Nombre de la clase: Ejercicio7
 *Fecha: 24-05-2018
 * Version: 1.0
 * Copyright: GPL/GNU
 * @author Carlos Lopez
 */
public class Ejercicio7 {
    private double num1;
    private double num2;
    public Ejercicio7() {
    }

    public double getNum1() {
        return num1;
    }

    public void setNum1(double num1) {
        this.num1 = num1;
    }

    public double getNum2() {
        return num2;
    }

    public void setNum2(double num2) {
        this.num2 = num2;
    }


    
    public void sumar(float nume1, float nume2){
        //utilizo datos de coma flotante debido a que
        //puede almacenar mas valores que float
        float suma=0;        
        suma = nume1 + nume2;
        JOptionPane.showMessageDialog(null, "La suma es: "+suma);
    }
    public void restar(float nume1, float nume2){
        float resta=0;
        resta = nume1 - nume2;
        JOptionPane.showMessageDialog(null, "La Restar es: "+resta);
    }
    public void multiplicar(float nume1, float nume2){
        float multi=0;
        multi = nume1 * nume2;
        JOptionPane.showMessageDialog(null, "La Multiplicacion es: "+multi);
    }
    public void dividir(float nume1, float nume2){
        float dividir=0;
        dividir = nume1 / nume2;
        JOptionPane.showMessageDialog(null, "La divicion es: "+dividir);
    }
    public void pow(double nume1, double nume2){
        //en este no uso datos float porque da error :V
        double pow=0;
        pow = Math.pow(nume1, nume2);
        JOptionPane.showMessageDialog(null, "La Potencia es: "+pow);
    }
    public void sqrt(double nume1, double nume2){
     double sqrt=0;
     sqrt = Math.pow(nume1,1/nume2);
     JOptionPane.showMessageDialog(null, "La Raiz "+nume2+" de " +nume1 + " es: "+sqrt);
    }
}
